<?php
include_once 'includes/header.php';
?>
<div class="row">
    <div class="col s12 m6 push-m3">
      <h3 class="light">Novo cliente</h3>
      <form action="php_action/create.php" method="POST">
      	<div class="input-field col s12">
      		<input type="text" name="name" id="name">
      		<label for="name">Name</label>
      	</div>
      	<div class="input-field col s12">
      		<input type="text" name="last_name" id="last_name">
      		<label for="last_name">Last Name</label>
      	</div>
      	<div class="input-field col s12">
      		<input type="text" name="email" id="email">
      		<label for="email">E-mail</label>
      	</div>
      	<div class="input-field col s12">
      		<input type="text" name="age" id="age">
      		<label for="age">Age</label>
      	</div>
      	<button type="submit" name="btn-save" class="btn">Save</button>
      	<a href="index.php" class="btn green">Clients list</a>
      </form>
    </div>
</div>

<?php
include_once 'includes/footer.php';
?>


    
