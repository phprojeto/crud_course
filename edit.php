<?php
require_once 'php_action/db_connect.php';

include_once 'includes/header.php';
if(isset($_GET['id'])){
	$id = mysqli_escape_string($connect, $_GET['id']);
	
	$sql = "SELECT * FROM clients WHERE id = '$id'";
      $resultado = mysqli_query($connect, $sql);
      $row = mysqli_fetch_assoc($resultado);

}

?>
<div class="row">
    <div class="col s12 m6 push-m3">
      <h3 class="light">Editar cliente</h3>
      <form action="php_action/update.php?id=<?php echo $_GET['id'] ?>" method="POST">
      	<div class="input-field col s12">
      		<input type="text" name="name" id="name" value="<?php echo $row['name']?>">
      		<label for="name">Name</label>
      	</div>
      	<div class="input-field col s12">
      		<input type="text" name="last_name" id="last_name" value="<?php echo $row['last_name']?>">
      		<label for="last_name">Last Name</label>
      	</div>
      	<div class="input-field col s12">
      		<input type="text" name="email" id="email" value="<?php echo $row['email']?>">
      		<label for="email">E-mail</label>
      	</div>
      	<div class="input-field col s12">
      		<input type="text" name="age" id="age" value="<?php echo $row['age']?>">
      		<label for="age">Age</label>
      	</div>
      	<button type="submit" name="btn-save" class="btn">Save</button>
      	<a href="index.php" class="btn green">Clients list</a>
      </form>
    </div>
</div>

<?php
include_once 'includes/footer.php';
?>