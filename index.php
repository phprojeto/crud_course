<?php
include_once 'includes/message.php';
//conexão
require_once 'php_action/db_connect.php';
//header
include_once 'includes/header.php';

$sql = "SELECT * FROM clients";
$resultado = mysqli_query($connect, $sql);
?>
<div class="row">
    <div class="col s12 m6 push-m3">
      <h3 class="light"> Clientes</h3>
      <table class="stripped">
        <thead>
          <tr>
            <th>Nome:</th>
            <th>Sobrenome:</th>
            <th>Email:</th>
            <th>Idade:</th>
            <th>Acoes:</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(mysqli_num_rows($resultado) > 0){
            while($row = mysqli_fetch_assoc($resultado)){
          ?>
              <tr>
                <td><?php echo $row['name']?></td>
                <td><?php echo $row['last_name']?></td>
                <td><?php echo $row['email']?></td>
                <td><?php echo $row['age']?></td>
                <td>
                  <a href="edit.php?id=<?php echo $row['id']?>" class="btn-floating orange">
                    <i class="material-icons">edit</i>
                  </a>
                  <a href="php_action/delete.php?id=<?php echo $row['id']?>" class="btn-floating red">
                    <i class="material-icons">delete</i>
                  </a>
                </td>
              </tr>
        <?php
          }
        } else {
        ?>

        <tr>
          <td colspan="5" style="text-align: center">Clients not found</td>
        </tr>
        <?php
      }
        ?>
      </tbody>
      </table>
      <br>
      <a href="add.php" class="btn">Adicionar clientes</a>
    </div>
</div>

<?php
include_once 'includes/footer.php';
?>


    
        